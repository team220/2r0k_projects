﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DriftPointCount : MonoBehaviour {


	private Car_Physics cp;

	private Transform wflc;
	private Transform wfrc;
	private Transform wbrc;
	private Transform wblc;
	public Text scoreTest;
	public GameObject textCube;
	Vector3 testCubeStartPos;
	float score = 0;


	void Start () {
		cp = gameObject.GetComponent<Car_Physics> ();
		wflc = cp.WColForward [0].transform;
		wfrc = cp.WColForward [1].transform;
		wbrc = cp.WColBack [0].transform;
		wblc = cp.WColBack [1].transform;

		testCubeStartPos = textCube.transform.position - (wflc.position + wfrc.position) / 2;
	}
	
	void FixedUpdate () {
//		Vector3 frontWheelsCenter = (wflc.position + wfrc.position) / 2;
//		Vector3 backWheelsCenter = (wbrc.position + wblc.position) / 2;
//		Vector3 result = frontWheelsCenter -

	//	textCube.transform.position = (wflc.position + wfrc.position) / 2 + testCubeStartPos;
		textCube.transform.position = (wflc.position + wfrc.position) / 2 + testCubeStartPos;
		Debug.DrawRay (textCube.transform.position, -GetComponent<Rigidbody> ().velocity);
		Debug.DrawRay (textCube.transform.position, transform.position - textCube.transform.position);

		Vector3 a = transform.position - textCube.transform.position;
		Vector3 b = -GetComponent<Rigidbody> ().velocity;
		float cos = (a.x * b.x + a.z * b.z)/((Mathf.Sqrt(Mathf.Pow(a.x,2) + Mathf.Pow(a.z,2)) *  (Mathf.Sqrt(Mathf.Pow(b.x,2) + Mathf.Pow(b.z,2)))));
		float angle = Mathf.Round(Mathf.Acos (cos) * 57.29f);
		if (Mathf.Sqrt(Mathf.Pow(b.x,2) + Mathf.Pow(b.z,2)) > 1 && angle > 20 && angle <90) {
			score+=Mathf.Round(angle/10);
		}

		scoreTest.text = "SCORE: " + score.ToString();
	}
}
