﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class AdsControl : MonoBehaviour {

	public string unityAdsID;
	private bool firstIntance = false;
	public bool FirstIntance { get { return firstIntance; } private set{firstIntance = value;} }

	//public GameObject adsPanel;

	void Awake () {
		if (!Advertisement.isInitialized) {
			if(firstIntance)
				return;
			if(Advertisement.isSupported)
				Advertisement.Initialize (unityAdsID);
			DontDestroyOnLoad (gameObject);
			firstIntance = true;
		} else {
			Destroy (gameObject);
		}

	}

	public static void ShowAds(){
		if (adsDelay > 0)
			return;
		adsDelay = 22f;
		//showAds = true;
	//	if (GUI.Button (new Rect (10, 10, 150, 50), Advertisement.IsReady () ? "Show Ad" : "Waiting...")) {
			Advertisement.Show (null, new ShowOptions {
			resultCallback = result => {
				Debug.Log(result.ToString());



			}
		});

	}
	//}

	static bool showAds = false;

	float timer1sec = 0f;
	float timer8sec = 0f;

	static float adsDelay = -1f;

	void Update(){
		if (adsDelay > 0 && !Advertisement.isShowing) {
			adsDelay -= Time.deltaTime;
		}
	}


	/*
	void Update(){
		if (showAds) {
			Debug.LogError ("Update");
			adsPanel.SetActive (true);
			adsPanel.transform.FindChild("LoadingAds").gameObject.SetActive(true);
			adsPanel.transform.FindChild("Timer").gameObject.SetActive(true);
			adsPanel.transform.FindChild("NoAds").gameObject.SetActive(true);
			adsPanel.transform.FindChild("ThankYou").gameObject.SetActive(false);
			timer8sec = 8f;
			timer1sec = 1f;
		
			showAds = false;
		}

		if (timer8sec > 0) {
			timer8sec -= Time.deltaTime;
			adsPanel.transform.FindChild ("Timer").GetComponent<Text> ().text = Mathf.Round (timer8sec).ToString ();

		} else if (timer1sec > 0 && adsPanel.transform.FindChild ("Timer").gameObject.activeInHierarchy) {
			adsPanel.transform.FindChild ("LoadingAds").gameObject.SetActive (false);
			adsPanel.transform.FindChild ("Timer").gameObject.SetActive (false);
			adsPanel.transform.FindChild ("NoAds").gameObject.SetActive (false);
			adsPanel.transform.FindChild ("ThankYou").gameObject.SetActive (true);
		} else if (timer1sec > 0 && !adsPanel.transform.FindChild ("Timer").gameObject.activeInHierarchy && !Advertisement.isShowing) {
			timer1sec -= Time.deltaTime;
		} else if (timer1sec <= 0 && adsPanel.activeInHierarchy) {
			adsPanel.SetActive(false);
		}
	   }
	*/
}
