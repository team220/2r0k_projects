﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class CheckPointsControl : MonoBehaviour {

	public int laps = 4;
	public int currentLap = 1;
	public Collider[] checkPoints;
	private int nextCp = 0;
	private int cpAmount;
	public LevelController lc;

	public void Start(){
		cpAmount = 4 * checkPoints.Length;
	}

	public void CheckPointAdd(Collider cp){
		Debug.Log (Array.IndexOf (checkPoints, cp));
		if (Array.IndexOf (checkPoints, cp) == nextCp) {
			nextCp = GetCorrectCheckPonitIndex (nextCp + 1);
			cpAmount--;
			lc.CheckPointPassed();//checkPointPassed
		} else if(Array.IndexOf (checkPoints, cp) == nextCp - 1){

		}else {
			nextCp = GetCorrectCheckPonitIndex (nextCp - 1);
			cpAmount++;
			lc.WrongDirection();//wrongDirection
		}

	
		if((cpAmount + 1) % laps == 0 && cpAmount / checkPoints.Length < laps){
			lc.LapDone();//lap done
			currentLap++;
		}

		if (cpAmount < 0) {
			lc.Win();//win;
		}
	}

	public int GetCorrectCheckPonitIndex(int cp)
	{
		if (cp > checkPoints.Length - 1) {
			cp = 0;
		}
		if (cp < 0) {
			cp = checkPoints.Length - 1;
		}
		return cp;
	}





}
