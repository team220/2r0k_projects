﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {

	void OnTriggerExit(Collider other){
		if (other.tag == "Player") {
			FindObjectOfType<CheckPointsControl>().CheckPointAdd(gameObject.GetComponent<Collider>());
		}
	}
}
