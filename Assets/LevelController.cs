﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelController : MonoBehaviour {

	public GameObject wrondDrirection;
	public Text lapCount;
	public CheckPointsControl cpControl;
	public string lapText = "LAP: ";
	public GameObject pausePanel;
	public GameObject awesomeText;
	public GameObject rusumeButton;


	void Start () {
		UpateLapLable ();
		wrondDrirection.SetActive (false);
		OnResume ();
	}
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && Application.loadedLevel != 0) {
			if(!pausePanel.activeInHierarchy){
				OnPause();
			}else{
				OnBackToMenu();
			}
		}
	}

	public void CheckPointPassed(){
		wrondDrirection.SetActive (false);
	}

	public void WrongDirection(){
		wrondDrirection.SetActive (true);
	}

	public void LapDone(){

		UpateLapLable ();
	}
	

	public void Win(){
		OnPause ();
		awesomeText.SetActive (true);
	}

	void UpateLapLable(){
		lapCount.text = lapText + " " + cpControl.currentLap + "/" + cpControl.laps;
	}


	public void OnPause(){
		Time.timeScale = 0;
		awesomeText.SetActive (false);
		pausePanel.SetActive (true);

	}

	public void OnResume(){
		Time.timeScale = 1;
		awesomeText.SetActive (false);
		pausePanel.SetActive (false);

	}

	public void OnRestart(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void OnBackToMenu(){
		Application.LoadLevel ("Main");
	}
}
