﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	public bool rotate = true;
	public float speed = 10f;
	public bool lookToThePlayer = false;

	
	void Update () {
		if (lookToThePlayer) {
			transform.LookAt (-GameObject.FindGameObjectWithTag ("Player").transform.position);
		} else {
			transform.Rotate (new Vector3 (0, speed * Time.deltaTime, 0));
		}
	}
}
