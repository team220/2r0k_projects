﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;



public class SmartponeController : MonoBehaviour {
	protected bool speedFlaq=true;
	protected bool firstTimeFlaq=true;


	public GameObject setupMenu;
	public GameObject rButton;
	public GameObject dButton;


	void Start () {
		rButton.SetActive(false);
		dButton.SetActive(true);
	}

	void Update () {
		Time.timeScale = 1;
		if((firstTimeFlaq)&(Advertisement.isInitialized)){
			AdsControl.ShowAds ();
			firstTimeFlaq=false;
		}

		if (Input.GetKeyDown (KeyCode.Escape) && Application.loadedLevel == 0) {
			if(!setupMenu.activeInHierarchy){
				Setups();
			}else{
				OutSetups();
			}
		}

//		if (Time.frameCount % 60 == 0) {
//			Application.CaptureScreenshot (Time.frameCount.ToString () + ".png");
//		}
	}

	public void SpeedChanger(){
		if (speedFlaq) {
			speedFlaq = false;
			rButton.SetActive(true);
			dButton.SetActive(false);
		} else {
			speedFlaq = true;
			rButton.SetActive(false);
			dButton.SetActive(true);
		}
	}

	public void TurnLeft(){
		Car_Physics.steer = -1.0f;
	}
	public void TurnLeftCanceled(){
		Car_Physics.steer = 0.0f;
	}
	public void TurnRight(){
		Car_Physics.steer = 1.0f;
	}
	public void TurnRightCanceled(){
		Car_Physics.steer = 0.0f;
	}

	public void Speed(){
		if (speedFlaq) {
			Car_Physics.accel = 1.0f;
		} else {
			Car_Physics.accel = -1.0f;
		}
	}

	public void SpeedCancel(){
		Car_Physics.accel = 0.0f;		
	}

	public void Setups(){
		Time.timeScale = 0;
		setupMenu.SetActive (true);	
	}
	
	public void OutSetups(){
		Time.timeScale = 1;
		setupMenu.SetActive (false);
	}

	public void Retry(){
			setupMenu.SetActive (false);
			Time.timeScale = 1;
			Application.LoadLevel (0);

	}

	public void RateUs(){
		Application.OpenURL ("https://play.google.com/store/apps/details?id=com.PotatoFreeGames.AbsoluteRedCubeDrift");
	}
		
}