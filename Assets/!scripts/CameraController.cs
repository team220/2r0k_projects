﻿using UnityEngine;
using System.Collections;
//using UnityEditor;


public class CameraController : MonoBehaviour {
	
	public GameObject player;
	
	public Vector3 offset = new Vector3(8.3f, 15.32f, -3.1f);
	
	void Start ()
	{
		//offset = transform.position - player.transform.position;
	}

	public void BackToDefault(){
		offset = new Vector3(8.3f, 15.32f, -3.1f);
	}
	
	void LateUpdate ()
	{
		transform.position = player.transform.position + offset;
		
	}

}

/*
//[CustomEditor(typeof(CameraController))]
public class CameraControllerInspector : Editor
{
	public override void OnInspectorGUI()
	{
		//DrawDefaultInspector ();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Back to default"))
		{
			var camControl = target as CameraController;
			if (camControl != null)
			{
				camControl.BackToDefault();
			}
		}
		GUILayout.EndHorizontal();
	}
}
*/
